let canvas;
let context;
let isDrawing;
let prevColorElement;
let prevThicknessElement;

window.onload = function () {
    canvas = document.getElementById('paper');
    context = canvas.getContext("2d");

    canvas.onmousedown = startDrawing;
    canvas.onmouseup = stopDrawing;
    canvas.onmouseout = stopDrawing;
    canvas.onmousemove = draw;
}

function changeColor(color, colorRed) {
    context.strokeStyle = color;
    prevColorElement = colorRed;
}


function rangeValue() {
    let value = document.getElementById("range").value;
    context.lineWidth = value;
}

function startDrawing(e) {
    isDrawing = true;

    context.beginPath();
    context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function draw(e) {
    if (isDrawing === true) {
        const x = e.pageX - canvas.offsetLeft;
        const y = e.pageY - canvas.offsetTop;

        context.lineTo(x, y);
        context.stroke();
    }
}

function stopDrawing() {
    isDrawing = false;
}